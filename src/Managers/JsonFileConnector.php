<?php

namespace App\Managers;

class JsonFileConnector
{
    private $loaded = false;
    private $content = '';
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->loaded = true;
        $this->filePath = $filePath;

        $this->content = file_get_contents($this->filePath);
    }

    public function getFileContent() : string
    {
        if (!$this->checkWorkStatus('loaded')) {
            throw new \Exception("Cant load file content when file isnt loaded");
        }


        return $this->content;
    }

    public function getContentAsArray() : array
    {
        if (!$this->checkWorkStatus('content')) {
            throw new \Exception("Cant get file content when file isnt loaded or content isnt loaded");
        }

       return json_decode($this->content, true);
    }

    public function getContentAsObject()
    {
        if (!$this->checkWorkStatus('content')) {
            throw new \Exception("Cant get file content when file isnt loaded or content isnt loaded");
        }

       return json_decode($this->content);
    }



    public function checkWorkStatus(string $statusToCheck) : bool
    {
        switch ($statusToCheck) {
            case "content":
                $actualStatus = $this->loaded && $this->content;
                break;
            case "loaded":
                $actualStatus = $this->loaded !== false;
                break;
            default:
                throw new \Exception("Wrong status");
        }

        return $actualStatus;
    }
}