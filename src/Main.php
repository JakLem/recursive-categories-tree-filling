<?php

namespace App;

use App\Managers\JsonFileConnector;
use App\Service\CategoryTreeManager;

class Main
{
    public function initialize()
    {
        $jsonConnector = new JsonFileConnector("json/tree.json");
        $treeContent = $jsonConnector->getContentAsObject();

        $jsonConnector = new JsonFileConnector("json/list.json");
        $listContent = $jsonConnector->getContentAsObject();

        $jsonFiller = new CategoryTreeManager($listContent, $treeContent);
        $ready = $jsonFiller->fillCategoryTreeWithNames();

	    file_put_contents("completeCategoriesTree.json", json_encode($ready));
    }
}