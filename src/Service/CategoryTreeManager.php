<?php


namespace App\Service;


class CategoryTreeManager
{
    // uzupełniamy tablice $tree danymi z tablicy $list
    /**
     * @var array
     */
    private array $categories;

	private array $list;
	private array $tree;

	public function __construct(array $list, array $tree)
    {
	    $this->list = $list;
	    $this->tree = $tree;
    }

	public function fillCategoryTreeWithNames() : array
    {
        $readyJson = [];
        $this->categories = $this->makeCategoriesListName();

        $readyArray = $this->displayArrayRecursively($this->tree);

        return $readyArray;
    }

    private function makeCategoriesListName() : array
    {
        $categories = [];
        foreach ($this->list as $category) {
            $categories[$category->category_id] = $category->translations->pl_PL->name;
        }

        return $categories;
    }

    private function getCategoryNameById(int $id) : string
    {
        return $this->categories[$id] ?? 'no-name';
    }

    //  Output array
	private function displayArrayRecursively($tree) {
		foreach ($tree as $key => $element) {
			if (is_object($element)) {
				$element->name = $this->getCategoryNameById($element->id);
				$tree[$key] = $element;

				if (!empty($element->children)) {
					$this->displayArrayRecursively($element->children);
				}
			}
		}

		return $tree;
	}



}