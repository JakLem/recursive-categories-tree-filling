<?php


namespace Tests;


use App\Managers\JsonFileConnector;
use PHPUnit\Framework\TestCase;

class JsonFileConnectorTest extends TestCase
{
    /**
     * @covers \App\Managers\JsonFileConnector
     */
    public function testShouldReturnJsonTextContentFromFile()
    {
        //Given
        $jsonConnector = new JsonFileConnector("tests/json/test.json");
        $jsonContent = $jsonConnector->getFileContent();
        $expectedJson = '{"0":"file","1":123,"2":"auto","many":[1,2,3]}';

        //Then
        $this->assertEquals($expectedJson, $jsonContent);
    }

    /**
     * @covers \App\Managers\JsonFileConnector
     */
    public function testShouldReturnJsonAsArrayFromFile()
    {
        //Given
        $jsonConnector = new JsonFileConnector("tests/json/test.json");
        $jsonArray = $jsonConnector->getContentAsArray();
        $expectedJsonArray = $array = array(
            "file", 123, "auto", "many" => [1, 2, 3]
        );

        //When

        //Then
        $this->assertEquals($expectedJsonArray, $jsonArray);

    }
}