<?php


namespace Tests;


use App\Managers\JsonFileConnector;
use App\Service\CategoryTreeManager;
use PHPUnit\Framework\TestCase;

class CategoryTreeManagerTest extends TestCase
{
	/**
	 * @covers \App\Service\CategoryTreeManager
	 */
	public function testShouldReturnCompleteCategoryTreeWithNames()
	{
	    //Given
		$jsonConnector = new JsonFileConnector("tests/json/tree-test.json");
		$tree = $jsonConnector->getContentAsObject();

		$jsonConnector = new JsonFileConnector("tests/json/cat-names.json");
		$categoriesNames = $jsonConnector->getContentAsObject();

		$categoryTreeManager = new CategoryTreeManager($categoriesNames, $tree);

	    //When
	    $readyArray = $categoryTreeManager->fillCategoryTreeWithNames();
		$categoryElement = $readyArray[0];

	    //Then
		$this->assertEquals(34, $categoryElement->id);
		$this->assertEquals("no-name", $categoryElement->name);
		$this->assertIsArray($categoryElement->children);
		$this->assertEquals("no-name", $categoryElement->children[0]->name);
	}
}