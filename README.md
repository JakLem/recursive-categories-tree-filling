Application is filling Tree Category in tree.json file with names from list.json file.

At first run ```composer install``` to download all dependencies\
If you want to run tests just type ```./vendor/bin/phpunit```\
To run application just type ```php index.php``` in console.\
After you run application file  will be generated with complete categories name ```completeCategoriesTree.json```